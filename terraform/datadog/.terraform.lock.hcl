# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/datadog/datadog" {
  version = "3.38.0"
  hashes = [
    "h1:yezGkTVCTverHgwZ9WIHuHbnEdTuHgIETPFXB9cYwm0=",
    "zh:1767b5149d1219b1fac4d32102970c245324b825acfba44e66d833de3b8729f7",
    "zh:30068a06216a9dd085f72759e8a1b2b902e6459b634fbd969fe025db2b96ac7f",
    "zh:3bb0c050a79b178cb28342b8638e3d4e716f83a76ef29a87c554fd56ddfe69ed",
    "zh:4b09ae52486435fc0e46c336c9908c35d4ac3007fcabb084942730be9d80b723",
    "zh:4cb0eccb21d51215cde4b29c1729288f16b5b8a12648f8aca8f204e888f8ba94",
    "zh:63fd60755f8f9cd179a09b6f08b2a161635ba589017d7335da9c28c4dcdaa2a2",
    "zh:7f40478774e11e8fea1a993f7e06f60d328a3feeeb38c7d4ae43db2abf6ebcd4",
    "zh:7fece3220a4d50d21e968b83892033bcb34ea8abe70510d85a7524e2372ee41e",
    "zh:8259c978a50881eb6a66c1a1d7022bb5fca95e37b6195d8829464b348f83d069",
    "zh:87a5d7381282e4f2bd51a34fb976d5091f245c08d7075371ddb7122d52b7d9a5",
    "zh:aca0c706f6d30989af3480ee1aaf6c3cef9c7e9b70d2a848cd5cc088b4d6317a",
    "zh:c15a822fc93f7cec3cfdbbe9bbb7087683f8094fc3b76803ab03e9ec3ee9931a",
    "zh:f87e4120509f4c9834c668969a391ae089bc22982b9a17ddc7ffcf7540cb75be",
    "zh:f96dcac5bfaa92e168f3fedbfe19c25968de7f5e17201820155df00b19a312d1",
  ]
}
