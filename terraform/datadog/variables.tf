variable "dd_api_key" {
    type = string
}
variable "dd_app_key" {
    type = string
}
variable "api_url" {
    default = "https://us5.datadoghq.com"
}