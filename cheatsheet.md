# cheet sobre git

## sobre

Conteudo criado para teste

## Conteúdo

### Git config

comando para configurar o usuário

```bash
    git config --global user.name "Nome do Usuário"
```

comando para configurar o editor de texto

```bash
    sudo git config --system core.editor vim
```

comando para editar a configuração via arquivo (~/.gitconfig)

```bash
    git config --global --edit
```
